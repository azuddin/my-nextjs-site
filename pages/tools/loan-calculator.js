import ReactECharts from "echarts-for-react";
import React, { useState } from "react";

class LoanCalculator extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loanAmount: 500000,
      interestRate: 3.5,
      termType: 12,
      loanTerm: 30,
      estPrincipal: 0,
      estInterest: 0,
      totalPayment: 0,
      totalInterest: 0,
      monthlyPayment: 0,

      interestArray: [],
      principalArray: [],
      principalArrayCum: [],
      loanBalanceArray: [],
    };
  }

  componentDidMount() {
    this.calculate();
  }

  getPmt = (rate, tenure, loan) => {
    loan = +loan;
    if (rate == 0 || tenure == 0 || loan == 0) return 0;
    return (
      (rate * -loan * Math.pow(1 + rate, tenure)) /
      (1 - Math.pow(1 + rate, tenure))
    );
  };

  calculate = (event) => {
    event?.preventDefault();
    const P = this.state.loanAmount; // principle or initial amount borrowed
    const i = this.state.interestRate / 100 / 12; // monthly interest rate
    const n = this.state.loanTerm * this.state.termType; // number of payments months

    // // M = P[i(1 + i) ^ n] / [(1 + i) ^ (n - 1)];
    // const m = ((P * i * Math.pow(1 + i, n)) / (Math.pow(1 + i, n) - 1)).toFixed(
    //   0
    // );
    const m = this.getPmt(i, n, P);
    const iV = i * P;

    this.setState({
      monthlyPayment: m.toFixed(0),
      estInterest: iV.toFixed(0),
      estPrincipal: (m - iV).toFixed(0),
      totalPayment: (m * n).toFixed(0),
      totalInterest: (m * n - P).toFixed(0),
    });

    var beginBalance = P,
      remainingTenure = n,
      monthCounter = 1,
      interestPaymentForTenure = [],
      principalRepaymentForTenure = [],
      balanceRepaymentForTenure = [],
      repaymentForTenure = [];
    for (let monthIndex = 0; monthIndex < n; monthIndex++) {
      var interestRate = i,
        interestPaid = beginBalance * interestRate,
        interestPayment = beginBalance * interestRate,
        pmt = this.getPmt(i, remainingTenure, beginBalance),
        repayment = Math.min(pmt, beginBalance + interestPaid),
        principlePaid = repayment - interestPaid;
      beginBalance = beginBalance - principlePaid;
      remainingTenure = n - monthCounter;
      monthCounter++;

      balanceRepaymentForTenure.push(beginBalance);
      repaymentForTenure.push(pmt);
      principalRepaymentForTenure.push(pmt - interestPayment);
      interestPaymentForTenure.push(interestPayment);
    }
    var totalInterestPayments = interestPaymentForTenure.reduce(
      (acc, i) => acc + i,
      0
    );

    this.setState({ interestArray: interestPaymentForTenure });
    this.setState({ principalArray: principalRepaymentForTenure });

    const cumulativeSum = (
      (sum) => (value) =>
        (sum += value)
    )(0);

    this.setState({
      principalArrayCum: principalRepaymentForTenure.map(cumulativeSum),
    });
    this.setState({ loanBalanceArray: balanceRepaymentForTenure });

    // console.log(
    //   `${P}\n${i}\n${n}\nmonthly: ${m}, monthly interest rate: ${i}, loan term: ${n} month(s), monthly interest value: ${iV}`
    // );
  };

  onInputChange = (event) => {
    /** Note: setState is asynchronous, so need to use callback to calculate the updated value */
    this.setState(
      {
        [event.target.name]: event.target.value,
      },
      () => {
        this.calculate(event);
      }
    );
  };

  render() {
    return (
      <div className="container py-3">
        <h1>Housing Loan Calculator</h1>
        <div className="row">
          <div className="col-md-8 col-sm-7 mb-3">
            <form className="my-3" onSubmit={this.calculate}>
              <div className="mb-3 row">
                <label className="col-sm-4 col-form-label" htmlFor="loanAmount">
                  <strong>Loan Amount</strong>
                </label>
                <div className="col-sm-8">
                  <div className="input-group input-group-sm">
                    <span className="input-group-text">RM</span>
                    <input
                      id="loadAmount"
                      name="loanAmount"
                      type="number"
                      className="form-control"
                      placeholder="0"
                      value={this.state.loanAmount}
                      onChange={this.onInputChange}
                    />
                  </div>
                </div>
              </div>
              <div className="mb-3 row">
                <label
                  className="col-sm-4 col-form-label"
                  htmlFor="interestRate"
                >
                  <strong>Interest Rate (%)</strong>
                </label>
                <div className="col-sm-8">
                  <input
                    id="interestRate"
                    name="interestRate"
                    type="number"
                    className="form-control form-control-sm"
                    placeholder="0"
                    value={this.state.interestRate}
                    onChange={this.onInputChange}
                  />
                </div>
              </div>
              <div className="mb-3 row">
                <label className="col-sm-4 col-form-label" htmlFor="loanTerm">
                  <strong>Loan Term</strong>
                </label>
                <div className="col-sm-8">
                  <div className="input-group input-group-sm">
                    <select
                      id="termType"
                      name="termType"
                      className="form-select"
                      value={this.state.termType}
                      onChange={this.onInputChange}
                    >
                      <option value="1">Month</option>
                      <option value="12">Year</option>
                    </select>
                    <input
                      id="loanTerm"
                      name="loanTerm"
                      type="number"
                      className="form-control"
                      placeholder="0"
                      style={{ flex: 3 }}
                      value={this.state.loanTerm}
                      onChange={this.onInputChange}
                    />
                  </div>
                </div>
              </div>
              {/* <div className="d-grid gap-2">
                <button
                  className="btn btn-outline-primary btn-sm btn-justify"
                  type="submit"
                >
                  Calculate
                </button>
              </div> */}
            </form>
            <div className="card mb-3">
              <div className="card-body">
                <h4 className="mb-3">Summary</h4>
                <div className="d-flex justify-content-between">
                  <p>Starting Principal Payment</p>
                  <strong>
                    RM{" "}
                    {String(this.state.estPrincipal).replace(
                      /(.)(?=(\d{3})+$)/g,
                      "$1,"
                    )}
                  </strong>
                </div>
                <div className="d-flex justify-content-between">
                  <p>Starting Interest Payment</p>
                  <strong>
                    RM{" "}
                    {String(this.state.estInterest).replace(
                      /(.)(?=(\d{3})+$)/g,
                      "$1,"
                    )}
                  </strong>
                </div>
                <div className="d-flex justify-content-between">
                  <p>Monthly Payment</p>
                  <strong>
                    RM{" "}
                    {String(this.state.monthlyPayment).replace(
                      /(.)(?=(\d{3})+$)/g,
                      "$1,"
                    )}
                  </strong>
                </div>
                <div className="d-flex justify-content-between">
                  <p>Total Payment</p>
                  <strong>
                    RM{" "}
                    {String(this.state.totalPayment).replace(
                      /(.)(?=(\d{3})+$)/g,
                      "$1,"
                    )}
                  </strong>
                </div>
                <div className="d-flex justify-content-between">
                  <p>Total Interest Payment</p>
                  <strong>
                    RM{" "}
                    {String(this.state.totalInterest).replace(
                      /(.)(?=(\d{3})+$)/g,
                      "$1,"
                    )}
                  </strong>
                </div>
              </div>
            </div>
            <div className="card">
              <div className="card-body">
                <h4 className="mb-3">Amortization Chart</h4>
                <ReactECharts
                  option={{
                    tooltip: {
                      trigger: "axis",
                      axisPointer: {
                        type: "cross",
                        crossStyle: {
                          color: "#999",
                        },
                      },
                      formatter: (params) => {
                        const title = `<strong>${(
                          parseInt(params[0].axisValue) / 12
                        ).toFixed(0)}</strong> Year</br>`;
                        const content = params.reduce(
                          (acc, p) =>
                            acc +
                            `${p.marker}${p.seriesName}: <strong>${String(
                              p.value
                            ).replace(
                              /(.)(?=(\d{3})+$)/g,
                              "$1,"
                            )}</strong><br>`,
                          ""
                        );
                        return title + content;
                      },
                    },
                    legend: {
                      data: ["Interest", "Principal", "Balance"],
                    },
                    grid: {
                      left: "3%",
                      right: "4%",
                      bottom: "3%",
                      containLabel: true,
                    },
                    toolbox: {
                      feature: {
                        saveAsImage: {},
                      },
                    },
                    xAxis: {
                      type: "category",
                      boundaryGap: false,
                      data: [
                        ...Array(this.state.interestArray.length + 1).keys(),
                      ].slice(1),
                      axisLabel: {
                        formatter: (val) => {
                          return `${parseInt(val / 12)} Year`;
                        },
                      },
                    },
                    yAxis: {
                      type: "value",
                    },
                    series: [
                      {
                        name: "Interest",
                        type: "line",
                        data: this.state.interestArray.map(
                          (
                            (sum) => (value) =>
                              (sum += value).toFixed(0)
                          )(0)
                        ),
                        itemStyle: { color: "#f56c6c" },
                      },
                      {
                        name: "Principal",
                        type: "line",
                        data: this.state.principalArrayCum.map((i) =>
                          i.toFixed(0)
                        ),
                        itemStyle: { color: "#5470c6" },
                      },
                      {
                        name: "Balance",
                        type: "line",
                        data: this.state.loanBalanceArray.map((i) =>
                          i.toFixed(0)
                        ),
                        itemStyle: { color: "#91cc75" },
                      },
                    ],
                  }}
                />
              </div>
            </div>
          </div>
          <div className="col-md-4 col-sm-5">
            <div className="card">
              <div className="card-body">
                <h5 className="text-center">
                  Your estimated mortgage repayments:
                </h5>
                <ReactECharts
                  option={{
                    tooltip: {
                      trigger: "item",
                    },
                    legend: {
                      top: "5%",
                      left: "center",
                    },
                    series: [
                      {
                        type: "pie",
                        radius: ["60%", "70%"],
                        avoidLabelOverlap: false,
                        label: {
                          show: false,
                          position: "center",
                        },
                        emphasis: {
                          label: {
                            show: true,
                            fontSize: "15",
                            fontWeight: "bold",
                            formatter: function (obj) {
                              return `${obj.data.name}\nRM ${String(
                                obj.data.value
                              ).replace(/(.)(?=(\d{3})+$)/g, "$1,")}`;
                            },
                          },
                        },
                        labelLine: {
                          show: false,
                        },
                        data: [
                          {
                            value: this.state.estPrincipal,
                            name: "Principal",
                            itemStyle: { color: "#5470c6" },
                          },
                          {
                            value: this.state.estInterest,
                            name: "Interest",
                            itemStyle: { color: "#f56c6c" },
                          },
                        ],
                      },
                    ],
                  }}
                />
                <br />
                <small>
                  <strong>
                    RM{" "}
                    {String(this.state.estPrincipal).replace(
                      /(.)(?=(\d{3})+$)/g,
                      "$1,"
                    )}
                  </strong>{" "}
                  Starting principal payment per month
                </small>
                <br />
                <small>
                  <strong>
                    RM{" "}
                    {String(this.state.estInterest).replace(
                      /(.)(?=(\d{3})+$)/g,
                      "$1,"
                    )}
                  </strong>{" "}
                  Starting interest payment per month
                </small>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default LoanCalculator;

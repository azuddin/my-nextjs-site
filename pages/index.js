import Head from "next/head";
import styles from "../styles/Home.module.css";
import { useState } from "react";

export default function Home() {
  const [celebrateState, setCelebrateState] = useState(false);
  const [counterState, setCounterState] = useState(1);
  let end;
  let colors = ["#bb0000", "#eeeeee"];

  const onCount = (event) => {
    if (celebrateState) {
      event.preventDefault();
      return;
    }
    setCounterState(counterState + 0.1);
    if (counterState > 2) {
      setCounterState(1);

      setCelebrateState(true);
      end = Date.now() + 15 * 200;
      frame();
    }
  };

  function frame() {
    confetti({
      particleCount: 2,
      angle: 60,
      spread: 55,
      origin: { x: 0 },
      colors: colors,
    });
    confetti({
      particleCount: 2,
      angle: 120,
      spread: 55,
      origin: { x: 1 },
      colors: colors,
    });

    if (Date.now() < end) {
      requestAnimationFrame(frame);
      setCelebrateState(true);
    } else {
      setCelebrateState(false);
    }
  }

  return (
    <div className={styles.container}>
      <Head>
        <title>Azuddin.com</title>
        <meta name="description" content="A simple man with simple needs ✌" />
        <link rel="icon" href="/favicon.ico" />
        <script
          src="https://cdn.jsdelivr.net/npm/canvas-confetti@1.4.0/dist/confetti.browser.min.js"
          defer
        ></script>
      </Head>

      <h1 className={`${celebrateState ? "text-muted" : ""}`}>
        Azuddin.com{" "}
        <span
          className={styles.noselect}
          onClick={onCount}
          style={{
            display: "inline-block",
            cursor: `${celebrateState ? "default" : "pointer"}`,
            transform: `scale(${counterState})`,
          }}
        >
          🚀
        </span>
      </h1>
    </div>
  );
}
